/**
 * generated by Xtext 2.21.0
 */
package br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Model#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Model#getProcess <em>Process</em>}</li>
 * </ul>
 *
 * @see br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.AirflowModelPackage#getModel()
 * @model
 * @generated
 */
public interface Model extends EObject
{
  /**
   * Returns the value of the '<em><b>Configuration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Configuration</em>' containment reference.
   * @see #setConfiguration(Configuration)
   * @see br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.AirflowModelPackage#getModel_Configuration()
   * @model containment="true"
   * @generated
   */
  Configuration getConfiguration();

  /**
   * Sets the value of the '{@link br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Model#getConfiguration <em>Configuration</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Configuration</em>' containment reference.
   * @see #getConfiguration()
   * @generated
   */
  void setConfiguration(Configuration value);

  /**
   * Returns the value of the '<em><b>Process</b></em>' containment reference list.
   * The list contents are of type {@link br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Process}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Process</em>' containment reference list.
   * @see br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.AirflowModelPackage#getModel_Process()
   * @model containment="true"
   * @generated
   */
  EList<br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Process> getProcess();

} // Model
