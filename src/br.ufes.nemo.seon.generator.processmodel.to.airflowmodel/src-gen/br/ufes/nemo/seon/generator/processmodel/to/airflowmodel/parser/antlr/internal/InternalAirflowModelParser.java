package br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.services.AirflowModelGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalAirflowModelParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Configuration'", "'{'", "'}'", "'author:'", "'application:'", "'about:'", "'#'", "'ontology_entity:'", "'process'", "'task'", "'next:'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalAirflowModelParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalAirflowModelParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalAirflowModelParser.tokenNames; }
    public String getGrammarFileName() { return "InternalAirflowModel.g"; }



     	private AirflowModelGrammarAccess grammarAccess;

        public InternalAirflowModelParser(TokenStream input, AirflowModelGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Model";
       	}

       	@Override
       	protected AirflowModelGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModel"
    // InternalAirflowModel.g:64:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalAirflowModel.g:64:46: (iv_ruleModel= ruleModel EOF )
            // InternalAirflowModel.g:65:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalAirflowModel.g:71:1: ruleModel returns [EObject current=null] : ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_process_1_0= ruleProcess ) )* ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        EObject lv_configuration_0_0 = null;

        EObject lv_process_1_0 = null;



        	enterRule();

        try {
            // InternalAirflowModel.g:77:2: ( ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_process_1_0= ruleProcess ) )* ) )
            // InternalAirflowModel.g:78:2: ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_process_1_0= ruleProcess ) )* )
            {
            // InternalAirflowModel.g:78:2: ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_process_1_0= ruleProcess ) )* )
            // InternalAirflowModel.g:79:3: ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_process_1_0= ruleProcess ) )*
            {
            // InternalAirflowModel.g:79:3: ( (lv_configuration_0_0= ruleConfiguration ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==11) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalAirflowModel.g:80:4: (lv_configuration_0_0= ruleConfiguration )
                    {
                    // InternalAirflowModel.g:80:4: (lv_configuration_0_0= ruleConfiguration )
                    // InternalAirflowModel.g:81:5: lv_configuration_0_0= ruleConfiguration
                    {

                    					newCompositeNode(grammarAccess.getModelAccess().getConfigurationConfigurationParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_3);
                    lv_configuration_0_0=ruleConfiguration();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getModelRule());
                    					}
                    					set(
                    						current,
                    						"configuration",
                    						lv_configuration_0_0,
                    						"br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.AirflowModel.Configuration");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalAirflowModel.g:98:3: ( (lv_process_1_0= ruleProcess ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==17||LA2_0==19) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalAirflowModel.g:99:4: (lv_process_1_0= ruleProcess )
            	    {
            	    // InternalAirflowModel.g:99:4: (lv_process_1_0= ruleProcess )
            	    // InternalAirflowModel.g:100:5: lv_process_1_0= ruleProcess
            	    {

            	    					newCompositeNode(grammarAccess.getModelAccess().getProcessProcessParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_process_1_0=ruleProcess();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"process",
            	    						lv_process_1_0,
            	    						"br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.AirflowModel.Process");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleConfiguration"
    // InternalAirflowModel.g:121:1: entryRuleConfiguration returns [EObject current=null] : iv_ruleConfiguration= ruleConfiguration EOF ;
    public final EObject entryRuleConfiguration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConfiguration = null;


        try {
            // InternalAirflowModel.g:121:54: (iv_ruleConfiguration= ruleConfiguration EOF )
            // InternalAirflowModel.g:122:2: iv_ruleConfiguration= ruleConfiguration EOF
            {
             newCompositeNode(grammarAccess.getConfigurationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConfiguration=ruleConfiguration();

            state._fsp--;

             current =iv_ruleConfiguration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConfiguration"


    // $ANTLR start "ruleConfiguration"
    // InternalAirflowModel.g:128:1: ruleConfiguration returns [EObject current=null] : (otherlv_0= 'Configuration' ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= '{' ( (lv_application_3_0= ruleApplication ) ) ( (lv_about_4_0= ruleAbout ) ) ( (lv_author_5_0= ruleAuthor ) ) otherlv_6= '}' ) ;
    public final EObject ruleConfiguration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_6=null;
        EObject lv_application_3_0 = null;

        EObject lv_about_4_0 = null;

        EObject lv_author_5_0 = null;



        	enterRule();

        try {
            // InternalAirflowModel.g:134:2: ( (otherlv_0= 'Configuration' ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= '{' ( (lv_application_3_0= ruleApplication ) ) ( (lv_about_4_0= ruleAbout ) ) ( (lv_author_5_0= ruleAuthor ) ) otherlv_6= '}' ) )
            // InternalAirflowModel.g:135:2: (otherlv_0= 'Configuration' ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= '{' ( (lv_application_3_0= ruleApplication ) ) ( (lv_about_4_0= ruleAbout ) ) ( (lv_author_5_0= ruleAuthor ) ) otherlv_6= '}' )
            {
            // InternalAirflowModel.g:135:2: (otherlv_0= 'Configuration' ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= '{' ( (lv_application_3_0= ruleApplication ) ) ( (lv_about_4_0= ruleAbout ) ) ( (lv_author_5_0= ruleAuthor ) ) otherlv_6= '}' )
            // InternalAirflowModel.g:136:3: otherlv_0= 'Configuration' ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= '{' ( (lv_application_3_0= ruleApplication ) ) ( (lv_about_4_0= ruleAbout ) ) ( (lv_author_5_0= ruleAuthor ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getConfigurationAccess().getConfigurationKeyword_0());
            		
            // InternalAirflowModel.g:140:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalAirflowModel.g:141:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalAirflowModel.g:141:4: (lv_name_1_0= RULE_STRING )
            // InternalAirflowModel.g:142:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_5); 

            					newLeafNode(lv_name_1_0, grammarAccess.getConfigurationAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConfigurationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalAirflowModel.g:162:3: ( (lv_application_3_0= ruleApplication ) )
            // InternalAirflowModel.g:163:4: (lv_application_3_0= ruleApplication )
            {
            // InternalAirflowModel.g:163:4: (lv_application_3_0= ruleApplication )
            // InternalAirflowModel.g:164:5: lv_application_3_0= ruleApplication
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getApplicationApplicationParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_7);
            lv_application_3_0=ruleApplication();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"application",
            						lv_application_3_0,
            						"br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.AirflowModel.Application");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAirflowModel.g:181:3: ( (lv_about_4_0= ruleAbout ) )
            // InternalAirflowModel.g:182:4: (lv_about_4_0= ruleAbout )
            {
            // InternalAirflowModel.g:182:4: (lv_about_4_0= ruleAbout )
            // InternalAirflowModel.g:183:5: lv_about_4_0= ruleAbout
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getAboutAboutParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_8);
            lv_about_4_0=ruleAbout();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"about",
            						lv_about_4_0,
            						"br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.AirflowModel.About");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAirflowModel.g:200:3: ( (lv_author_5_0= ruleAuthor ) )
            // InternalAirflowModel.g:201:4: (lv_author_5_0= ruleAuthor )
            {
            // InternalAirflowModel.g:201:4: (lv_author_5_0= ruleAuthor )
            // InternalAirflowModel.g:202:5: lv_author_5_0= ruleAuthor
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getAuthorAuthorParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_9);
            lv_author_5_0=ruleAuthor();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"author",
            						lv_author_5_0,
            						"br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.AirflowModel.Author");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConfiguration"


    // $ANTLR start "entryRuleAuthor"
    // InternalAirflowModel.g:227:1: entryRuleAuthor returns [EObject current=null] : iv_ruleAuthor= ruleAuthor EOF ;
    public final EObject entryRuleAuthor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAuthor = null;


        try {
            // InternalAirflowModel.g:227:47: (iv_ruleAuthor= ruleAuthor EOF )
            // InternalAirflowModel.g:228:2: iv_ruleAuthor= ruleAuthor EOF
            {
             newCompositeNode(grammarAccess.getAuthorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAuthor=ruleAuthor();

            state._fsp--;

             current =iv_ruleAuthor; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAuthor"


    // $ANTLR start "ruleAuthor"
    // InternalAirflowModel.g:234:1: ruleAuthor returns [EObject current=null] : (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleAuthor() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalAirflowModel.g:240:2: ( (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalAirflowModel.g:241:2: (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalAirflowModel.g:241:2: (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalAirflowModel.g:242:3: otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,14,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getAuthorAccess().getAuthorKeyword_0());
            		
            // InternalAirflowModel.g:246:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalAirflowModel.g:247:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalAirflowModel.g:247:4: (lv_name_1_0= RULE_STRING )
            // InternalAirflowModel.g:248:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAuthorAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAuthorRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAuthor"


    // $ANTLR start "entryRuleApplication"
    // InternalAirflowModel.g:268:1: entryRuleApplication returns [EObject current=null] : iv_ruleApplication= ruleApplication EOF ;
    public final EObject entryRuleApplication() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleApplication = null;


        try {
            // InternalAirflowModel.g:268:52: (iv_ruleApplication= ruleApplication EOF )
            // InternalAirflowModel.g:269:2: iv_ruleApplication= ruleApplication EOF
            {
             newCompositeNode(grammarAccess.getApplicationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleApplication=ruleApplication();

            state._fsp--;

             current =iv_ruleApplication; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleApplication"


    // $ANTLR start "ruleApplication"
    // InternalAirflowModel.g:275:1: ruleApplication returns [EObject current=null] : (otherlv_0= 'application:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleApplication() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalAirflowModel.g:281:2: ( (otherlv_0= 'application:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalAirflowModel.g:282:2: (otherlv_0= 'application:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalAirflowModel.g:282:2: (otherlv_0= 'application:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalAirflowModel.g:283:3: otherlv_0= 'application:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,15,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getApplicationAccess().getApplicationKeyword_0());
            		
            // InternalAirflowModel.g:287:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalAirflowModel.g:288:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalAirflowModel.g:288:4: (lv_name_1_0= RULE_STRING )
            // InternalAirflowModel.g:289:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getApplicationAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getApplicationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleApplication"


    // $ANTLR start "entryRuleAbout"
    // InternalAirflowModel.g:309:1: entryRuleAbout returns [EObject current=null] : iv_ruleAbout= ruleAbout EOF ;
    public final EObject entryRuleAbout() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbout = null;


        try {
            // InternalAirflowModel.g:309:46: (iv_ruleAbout= ruleAbout EOF )
            // InternalAirflowModel.g:310:2: iv_ruleAbout= ruleAbout EOF
            {
             newCompositeNode(grammarAccess.getAboutRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbout=ruleAbout();

            state._fsp--;

             current =iv_ruleAbout; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbout"


    // $ANTLR start "ruleAbout"
    // InternalAirflowModel.g:316:1: ruleAbout returns [EObject current=null] : (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleAbout() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalAirflowModel.g:322:2: ( (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalAirflowModel.g:323:2: (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalAirflowModel.g:323:2: (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalAirflowModel.g:324:3: otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,16,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getAboutAccess().getAboutKeyword_0());
            		
            // InternalAirflowModel.g:328:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalAirflowModel.g:329:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalAirflowModel.g:329:4: (lv_name_1_0= RULE_STRING )
            // InternalAirflowModel.g:330:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAboutAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAboutRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbout"


    // $ANTLR start "entryRuleDescription"
    // InternalAirflowModel.g:350:1: entryRuleDescription returns [EObject current=null] : iv_ruleDescription= ruleDescription EOF ;
    public final EObject entryRuleDescription() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDescription = null;


        try {
            // InternalAirflowModel.g:350:52: (iv_ruleDescription= ruleDescription EOF )
            // InternalAirflowModel.g:351:2: iv_ruleDescription= ruleDescription EOF
            {
             newCompositeNode(grammarAccess.getDescriptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDescription=ruleDescription();

            state._fsp--;

             current =iv_ruleDescription; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDescription"


    // $ANTLR start "ruleDescription"
    // InternalAirflowModel.g:357:1: ruleDescription returns [EObject current=null] : (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleDescription() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_textfield_1_0=null;


        	enterRule();

        try {
            // InternalAirflowModel.g:363:2: ( (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) ) )
            // InternalAirflowModel.g:364:2: (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) )
            {
            // InternalAirflowModel.g:364:2: (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) )
            // InternalAirflowModel.g:365:3: otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,17,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getDescriptionAccess().getNumberSignKeyword_0());
            		
            // InternalAirflowModel.g:369:3: ( (lv_textfield_1_0= RULE_STRING ) )
            // InternalAirflowModel.g:370:4: (lv_textfield_1_0= RULE_STRING )
            {
            // InternalAirflowModel.g:370:4: (lv_textfield_1_0= RULE_STRING )
            // InternalAirflowModel.g:371:5: lv_textfield_1_0= RULE_STRING
            {
            lv_textfield_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_textfield_1_0, grammarAccess.getDescriptionAccess().getTextfieldSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDescriptionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"textfield",
            						lv_textfield_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDescription"


    // $ANTLR start "entryRuleOntology_Entity"
    // InternalAirflowModel.g:391:1: entryRuleOntology_Entity returns [EObject current=null] : iv_ruleOntology_Entity= ruleOntology_Entity EOF ;
    public final EObject entryRuleOntology_Entity() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOntology_Entity = null;


        try {
            // InternalAirflowModel.g:391:56: (iv_ruleOntology_Entity= ruleOntology_Entity EOF )
            // InternalAirflowModel.g:392:2: iv_ruleOntology_Entity= ruleOntology_Entity EOF
            {
             newCompositeNode(grammarAccess.getOntology_EntityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOntology_Entity=ruleOntology_Entity();

            state._fsp--;

             current =iv_ruleOntology_Entity; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOntology_Entity"


    // $ANTLR start "ruleOntology_Entity"
    // InternalAirflowModel.g:398:1: ruleOntology_Entity returns [EObject current=null] : (otherlv_0= 'ontology_entity:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleOntology_Entity() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalAirflowModel.g:404:2: ( (otherlv_0= 'ontology_entity:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalAirflowModel.g:405:2: (otherlv_0= 'ontology_entity:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalAirflowModel.g:405:2: (otherlv_0= 'ontology_entity:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalAirflowModel.g:406:3: otherlv_0= 'ontology_entity:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getOntology_EntityAccess().getOntology_entityKeyword_0());
            		
            // InternalAirflowModel.g:410:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalAirflowModel.g:411:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalAirflowModel.g:411:4: (lv_name_1_0= RULE_STRING )
            // InternalAirflowModel.g:412:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getOntology_EntityAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOntology_EntityRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOntology_Entity"


    // $ANTLR start "entryRuleProcess"
    // InternalAirflowModel.g:432:1: entryRuleProcess returns [EObject current=null] : iv_ruleProcess= ruleProcess EOF ;
    public final EObject entryRuleProcess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProcess = null;


        try {
            // InternalAirflowModel.g:432:48: (iv_ruleProcess= ruleProcess EOF )
            // InternalAirflowModel.g:433:2: iv_ruleProcess= ruleProcess EOF
            {
             newCompositeNode(grammarAccess.getProcessRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProcess=ruleProcess();

            state._fsp--;

             current =iv_ruleProcess; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProcess"


    // $ANTLR start "ruleProcess"
    // InternalAirflowModel.g:439:1: ruleProcess returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'process' ( (lv_name_2_0= RULE_STRING ) ) otherlv_3= '{' ( (lv_tasks_4_0= ruleTask ) )* otherlv_5= '}' ) ;
    public final EObject ruleProcess() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_description_0_0 = null;

        EObject lv_tasks_4_0 = null;



        	enterRule();

        try {
            // InternalAirflowModel.g:445:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'process' ( (lv_name_2_0= RULE_STRING ) ) otherlv_3= '{' ( (lv_tasks_4_0= ruleTask ) )* otherlv_5= '}' ) )
            // InternalAirflowModel.g:446:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'process' ( (lv_name_2_0= RULE_STRING ) ) otherlv_3= '{' ( (lv_tasks_4_0= ruleTask ) )* otherlv_5= '}' )
            {
            // InternalAirflowModel.g:446:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'process' ( (lv_name_2_0= RULE_STRING ) ) otherlv_3= '{' ( (lv_tasks_4_0= ruleTask ) )* otherlv_5= '}' )
            // InternalAirflowModel.g:447:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'process' ( (lv_name_2_0= RULE_STRING ) ) otherlv_3= '{' ( (lv_tasks_4_0= ruleTask ) )* otherlv_5= '}'
            {
            // InternalAirflowModel.g:447:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==17) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalAirflowModel.g:448:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalAirflowModel.g:448:4: (lv_description_0_0= ruleDescription )
                    // InternalAirflowModel.g:449:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getProcessAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_10);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getProcessRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.AirflowModel.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,19,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getProcessAccess().getProcessKeyword_1());
            		
            // InternalAirflowModel.g:470:3: ( (lv_name_2_0= RULE_STRING ) )
            // InternalAirflowModel.g:471:4: (lv_name_2_0= RULE_STRING )
            {
            // InternalAirflowModel.g:471:4: (lv_name_2_0= RULE_STRING )
            // InternalAirflowModel.g:472:5: lv_name_2_0= RULE_STRING
            {
            lv_name_2_0=(Token)match(input,RULE_STRING,FOLLOW_5); 

            					newLeafNode(lv_name_2_0, grammarAccess.getProcessAccess().getNameSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getProcessRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_11); 

            			newLeafNode(otherlv_3, grammarAccess.getProcessAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalAirflowModel.g:492:3: ( (lv_tasks_4_0= ruleTask ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==17||LA4_0==20) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalAirflowModel.g:493:4: (lv_tasks_4_0= ruleTask )
            	    {
            	    // InternalAirflowModel.g:493:4: (lv_tasks_4_0= ruleTask )
            	    // InternalAirflowModel.g:494:5: lv_tasks_4_0= ruleTask
            	    {

            	    					newCompositeNode(grammarAccess.getProcessAccess().getTasksTaskParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_11);
            	    lv_tasks_4_0=ruleTask();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getProcessRule());
            	    					}
            	    					add(
            	    						current,
            	    						"tasks",
            	    						lv_tasks_4_0,
            	    						"br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.AirflowModel.Task");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getProcessAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProcess"


    // $ANTLR start "entryRuleTask"
    // InternalAirflowModel.g:519:1: entryRuleTask returns [EObject current=null] : iv_ruleTask= ruleTask EOF ;
    public final EObject entryRuleTask() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTask = null;


        try {
            // InternalAirflowModel.g:519:45: (iv_ruleTask= ruleTask EOF )
            // InternalAirflowModel.g:520:2: iv_ruleTask= ruleTask EOF
            {
             newCompositeNode(grammarAccess.getTaskRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTask=ruleTask();

            state._fsp--;

             current =iv_ruleTask; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTask"


    // $ANTLR start "ruleTask"
    // InternalAirflowModel.g:526:1: ruleTask returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'task' ( (lv_name_2_0= RULE_STRING ) ) otherlv_3= '{' ( (lv_ontology_entity_4_0= ruleOntology_Entity ) ) ( (lv_next_5_0= ruleNext ) )? otherlv_6= '}' ) ;
    public final EObject ruleTask() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        EObject lv_description_0_0 = null;

        EObject lv_ontology_entity_4_0 = null;

        EObject lv_next_5_0 = null;



        	enterRule();

        try {
            // InternalAirflowModel.g:532:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'task' ( (lv_name_2_0= RULE_STRING ) ) otherlv_3= '{' ( (lv_ontology_entity_4_0= ruleOntology_Entity ) ) ( (lv_next_5_0= ruleNext ) )? otherlv_6= '}' ) )
            // InternalAirflowModel.g:533:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'task' ( (lv_name_2_0= RULE_STRING ) ) otherlv_3= '{' ( (lv_ontology_entity_4_0= ruleOntology_Entity ) ) ( (lv_next_5_0= ruleNext ) )? otherlv_6= '}' )
            {
            // InternalAirflowModel.g:533:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'task' ( (lv_name_2_0= RULE_STRING ) ) otherlv_3= '{' ( (lv_ontology_entity_4_0= ruleOntology_Entity ) ) ( (lv_next_5_0= ruleNext ) )? otherlv_6= '}' )
            // InternalAirflowModel.g:534:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'task' ( (lv_name_2_0= RULE_STRING ) ) otherlv_3= '{' ( (lv_ontology_entity_4_0= ruleOntology_Entity ) ) ( (lv_next_5_0= ruleNext ) )? otherlv_6= '}'
            {
            // InternalAirflowModel.g:534:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==17) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalAirflowModel.g:535:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalAirflowModel.g:535:4: (lv_description_0_0= ruleDescription )
                    // InternalAirflowModel.g:536:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getTaskAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_12);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getTaskRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.AirflowModel.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,20,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getTaskAccess().getTaskKeyword_1());
            		
            // InternalAirflowModel.g:557:3: ( (lv_name_2_0= RULE_STRING ) )
            // InternalAirflowModel.g:558:4: (lv_name_2_0= RULE_STRING )
            {
            // InternalAirflowModel.g:558:4: (lv_name_2_0= RULE_STRING )
            // InternalAirflowModel.g:559:5: lv_name_2_0= RULE_STRING
            {
            lv_name_2_0=(Token)match(input,RULE_STRING,FOLLOW_5); 

            					newLeafNode(lv_name_2_0, grammarAccess.getTaskAccess().getNameSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTaskRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_13); 

            			newLeafNode(otherlv_3, grammarAccess.getTaskAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalAirflowModel.g:579:3: ( (lv_ontology_entity_4_0= ruleOntology_Entity ) )
            // InternalAirflowModel.g:580:4: (lv_ontology_entity_4_0= ruleOntology_Entity )
            {
            // InternalAirflowModel.g:580:4: (lv_ontology_entity_4_0= ruleOntology_Entity )
            // InternalAirflowModel.g:581:5: lv_ontology_entity_4_0= ruleOntology_Entity
            {

            					newCompositeNode(grammarAccess.getTaskAccess().getOntology_entityOntology_EntityParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_14);
            lv_ontology_entity_4_0=ruleOntology_Entity();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTaskRule());
            					}
            					set(
            						current,
            						"ontology_entity",
            						lv_ontology_entity_4_0,
            						"br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.AirflowModel.Ontology_Entity");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAirflowModel.g:598:3: ( (lv_next_5_0= ruleNext ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==21) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalAirflowModel.g:599:4: (lv_next_5_0= ruleNext )
                    {
                    // InternalAirflowModel.g:599:4: (lv_next_5_0= ruleNext )
                    // InternalAirflowModel.g:600:5: lv_next_5_0= ruleNext
                    {

                    					newCompositeNode(grammarAccess.getTaskAccess().getNextNextParserRuleCall_5_0());
                    				
                    pushFollow(FOLLOW_9);
                    lv_next_5_0=ruleNext();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getTaskRule());
                    					}
                    					set(
                    						current,
                    						"next",
                    						lv_next_5_0,
                    						"br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.AirflowModel.Next");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getTaskAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTask"


    // $ANTLR start "entryRuleNext"
    // InternalAirflowModel.g:625:1: entryRuleNext returns [EObject current=null] : iv_ruleNext= ruleNext EOF ;
    public final EObject entryRuleNext() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNext = null;


        try {
            // InternalAirflowModel.g:625:45: (iv_ruleNext= ruleNext EOF )
            // InternalAirflowModel.g:626:2: iv_ruleNext= ruleNext EOF
            {
             newCompositeNode(grammarAccess.getNextRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNext=ruleNext();

            state._fsp--;

             current =iv_ruleNext; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNext"


    // $ANTLR start "ruleNext"
    // InternalAirflowModel.g:632:1: ruleNext returns [EObject current=null] : (otherlv_0= 'next:' ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleNext() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalAirflowModel.g:638:2: ( (otherlv_0= 'next:' ( (otherlv_1= RULE_ID ) ) ) )
            // InternalAirflowModel.g:639:2: (otherlv_0= 'next:' ( (otherlv_1= RULE_ID ) ) )
            {
            // InternalAirflowModel.g:639:2: (otherlv_0= 'next:' ( (otherlv_1= RULE_ID ) ) )
            // InternalAirflowModel.g:640:3: otherlv_0= 'next:' ( (otherlv_1= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,21,FOLLOW_15); 

            			newLeafNode(otherlv_0, grammarAccess.getNextAccess().getNextKeyword_0());
            		
            // InternalAirflowModel.g:644:3: ( (otherlv_1= RULE_ID ) )
            // InternalAirflowModel.g:645:4: (otherlv_1= RULE_ID )
            {
            // InternalAirflowModel.g:645:4: (otherlv_1= RULE_ID )
            // InternalAirflowModel.g:646:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getNextRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_1, grammarAccess.getNextAccess().getTypeTaskCrossReference_1_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNext"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x00000000000A0002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000122000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000202000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000020L});

}