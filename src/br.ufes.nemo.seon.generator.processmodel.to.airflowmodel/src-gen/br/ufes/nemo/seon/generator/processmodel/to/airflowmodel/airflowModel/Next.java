/**
 * generated by Xtext 2.21.0
 */
package br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Next</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Next#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.AirflowModelPackage#getNext()
 * @model
 * @generated
 */
public interface Next extends EObject
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' reference.
   * @see #setType(Task)
   * @see br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.AirflowModelPackage#getNext_Type()
   * @model
   * @generated
   */
  Task getType();

  /**
   * Sets the value of the '{@link br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Next#getType <em>Type</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' reference.
   * @see #getType()
   * @generated
   */
  void setType(Task value);

} // Next
