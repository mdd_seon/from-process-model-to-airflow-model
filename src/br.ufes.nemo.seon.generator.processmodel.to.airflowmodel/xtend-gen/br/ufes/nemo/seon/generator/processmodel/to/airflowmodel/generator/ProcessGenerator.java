package br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.generator;

import br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Configuration;
import br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Description;
import br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Next;
import br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Task;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class ProcessGenerator extends AbstractGenerator {
  private String PATH = "src/";
  
  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    final Iterable<Configuration> configurations = Iterables.<Configuration>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Configuration.class);
    final Configuration configuration = ((Configuration[])Conversions.unwrapArray(configurations, Configuration.class))[0];
    final String processName = configuration.getName().toLowerCase();
    final String application_name = configuration.getApplication().getName().toLowerCase();
    if (((!StringExtensions.isNullOrEmpty(processName)) && (!StringExtensions.isNullOrEmpty(application_name)))) {
      Iterable<br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Process> _filter = Iterables.<br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Process>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Process.class);
      for (final br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Process process : _filter) {
        String _lowerCase = processName.toLowerCase();
        String _plus = (this.PATH + _lowerCase);
        String _plus_1 = (_plus + ".py");
        fsa.generateFile(_plus_1, this.compile(process, processName, application_name));
      }
    }
  }
  
  public CharSequence compile(final br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Process process, final String processName, final String application_name) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("import airflow");
    _builder.newLine();
    _builder.append("from airflow.models import DAG");
    _builder.newLine();
    _builder.append("from airflow.operators.python_operator import PythonOperator");
    _builder.newLine();
    _builder.append("import logging");
    _builder.newLine();
    _builder.append("from ");
    _builder.append(application_name);
    _builder.append(".factory.factory import Factory");
    _builder.newLineIfNotEmpty();
    _builder.append("from ");
    _builder.append(application_name);
    _builder.append(".factory.service_enum import Service");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("logging.basicConfig(level=logging.INFO)");
    _builder.newLine();
    _builder.newLine();
    _builder.append("args = {");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("\'owner\': \'seon\',");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("\'start_date\': airflow.utils.dates.days_ago(1)");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("dag = DAG (");
    _builder.newLine();
    _builder.append("    ");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("dag_id= \'");
    _builder.append(processName, "    ");
    _builder.append("\',");
    _builder.newLineIfNotEmpty();
    _builder.append("    ");
    _builder.append("default_args=args,");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("schedule_interval = None,");
    _builder.newLine();
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("def __retrive_config(kwargs):");
    _builder.newLine();
    _builder.append("    ");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("conf = kwargs[\'dag_run\'].conf");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("organization_uuid = conf[\'organization_uuid\']");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("application_uuid = conf[\'application_uuid\']");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("secret = conf[\'secret\']");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("url = conf[\'url\']");
    _builder.newLine();
    _builder.newLine();
    _builder.append("    ");
    _builder.append("data = {\'organization_id\': organization_uuid, ");
    _builder.newLine();
    _builder.append("                        ");
    _builder.append("\"key\": secret, ");
    _builder.newLine();
    _builder.append("                        ");
    _builder.append("\"url\": url,");
    _builder.newLine();
    _builder.append("                        ");
    _builder.append("\"application\": application_uuid}  ");
    _builder.newLine();
    _builder.newLine();
    _builder.append("    ");
    _builder.append("return organization_uuid, data");
    _builder.newLine();
    _builder.newLine();
    _builder.append("## Functions");
    _builder.newLine();
    {
      EList<Task> _tasks = process.getTasks();
      for(final Task t : _tasks) {
        _builder.append("def retrive_");
        String _lowerCase = t.getName().toLowerCase();
        _builder.append(_lowerCase);
        _builder.append("(**kwargs):");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("try:");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("pprint (\"retrive ");
        String _lowerCase_1 = t.getOntology_entity().getName().toLowerCase();
        _builder.append(_lowerCase_1, "\t\t");
        _builder.append("\")");
        _builder.newLineIfNotEmpty();
        _builder.append("\t        ");
        _builder.newLine();
        _builder.append("\t    ");
        _builder.append("organization_uuid, data = __retrive_config(kwargs)");
        _builder.newLine();
        _builder.append("\t        ");
        _builder.newLine();
        _builder.append("\t    ");
        _builder.append("service = Factory.create(Service.");
        String _lowerCase_2 = t.getOntology_entity().getName().toLowerCase();
        _builder.append(_lowerCase_2, "\t    ");
        _builder.append(")");
        _builder.newLineIfNotEmpty();
        _builder.append("\t    ");
        _builder.append("service.integrate(organization_uuid,data)");
        _builder.newLine();
        _builder.append("\t");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("except Exception as e: ");
        _builder.newLine();
        _builder.append("\t    ");
        _builder.append("logging.error(\"OS error: {0}\".format(e))");
        _builder.newLine();
        _builder.append("\t    ");
        _builder.append("logging.error(e.__dict__)");
        _builder.newLine();
        _builder.append("\t");
        _builder.newLine();
      }
    }
    _builder.newLine();
    _builder.append("## Operators");
    _builder.newLine();
    {
      EList<Task> _tasks_1 = process.getTasks();
      for(final Task t_1 : _tasks_1) {
        {
          Description _description = t_1.getDescription();
          boolean _notEquals = (!Objects.equal(_description, null));
          if (_notEquals) {
            _builder.append("## ");
            String _textfield = t_1.getDescription().getTextfield();
            _builder.append(_textfield);
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("retrive_");
        String _lowerCase_3 = t_1.getName().toLowerCase();
        _builder.append(_lowerCase_3);
        _builder.append("_operator = PythonOperator(");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("task_id = \'retrive_");
        String _lowerCase_4 = t_1.getName().toLowerCase();
        _builder.append(_lowerCase_4, "\t");
        _builder.append("\',");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("provide_context=True,");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("python_callable=retrive_");
        String _lowerCase_5 = t_1.getName().toLowerCase();
        _builder.append(_lowerCase_5, "\t");
        _builder.append(", ");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("dag=dag,");
        _builder.newLine();
        _builder.append(")");
        _builder.newLine();
        _builder.newLine();
      }
    }
    _builder.append("## Workflow");
    _builder.newLine();
    {
      EList<Task> _tasks_2 = process.getTasks();
      for(final Task t_2 : _tasks_2) {
        _builder.append("retrive_");
        String _lowerCase_6 = t_2.getName().toLowerCase();
        _builder.append(_lowerCase_6);
        _builder.append("_operator ");
        {
          Next _next = t_2.getNext();
          boolean _notEquals_1 = (!Objects.equal(_next, null));
          if (_notEquals_1) {
            _builder.append(">>retrive_");
            String _name = t_2.getNext().getType().getName();
            _builder.append(_name);
            _builder.append("_operator");
          }
        }
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
}
