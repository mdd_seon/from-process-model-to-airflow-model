package br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.generator

import br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Configuration
import br.ufes.nemo.seon.generator.processmodel.to.airflowmodel.airflowModel.Process
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

class ProcessGenerator extends AbstractGenerator {
	
	var PATH = "src/"
	
	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {

		val configurations = resource.allContents.toIterable.filter(Configuration)
		val Configuration configuration = configurations.get(0)
		val String processName = configuration.name.toLowerCase
		val application_name = configuration.application.name.toLowerCase
		
		if (!processName.isNullOrEmpty  && !application_name.isNullOrEmpty)
		{
			for (process : resource.allContents.toIterable.filter(Process))
			{
				fsa.generateFile(PATH  +  processName.toLowerCase + ".py",process.compile(processName,application_name))
			}	
		}
	}
	// criando o process
	def compile (Process process, String processName, String application_name)'''
		import airflow
		from airflow.models import DAG
		from airflow.operators.python_operator import PythonOperator
		import logging
		from «application_name».factory.factory import Factory
		from «application_name».factory.service_enum import Service
		
		logging.basicConfig(level=logging.INFO)
		
		args = {
		    'owner': 'seon',
		    'start_date': airflow.utils.dates.days_ago(1)
		}
		
		dag = DAG (
		    
		    dag_id= '«processName»',
		    default_args=args,
		    schedule_interval = None,
		)
		
		def __retrive_config(kwargs):
		    
		    conf = kwargs['dag_run'].conf
		    organization_uuid = conf['organization_uuid']
		    application_uuid = conf['application_uuid']
		    secret = conf['secret']
		    url = conf['url']
		
		    data = {'organization_id': organization_uuid, 
		                        "key": secret, 
		                        "url": url,
		                        "application": application_uuid}  
		
		    return organization_uuid, data
		
		## Functions
		«FOR t: process.tasks»
			def retrive_«t.name.toLowerCase»(**kwargs):
				try:
					pprint ("retrive «t.ontology_entity.name.toLowerCase»")
				        
				    organization_uuid, data = __retrive_config(kwargs)
				        
				    service = Factory.create(Service.«t.ontology_entity.name.toLowerCase»)
				    service.integrate(organization_uuid,data)
				
				except Exception as e: 
				    logging.error("OS error: {0}".format(e))
				    logging.error(e.__dict__)
				
		«ENDFOR»
		
		## Operators
		«FOR t: process.tasks»
			«IF t.description!= null»
				## «t.description.textfield»
			«ENDIF»
			retrive_«t.name.toLowerCase»_operator = PythonOperator(
				task_id = 'retrive_«t.name.toLowerCase»',
				provide_context=True,
				python_callable=retrive_«t.name.toLowerCase», 
				dag=dag,
			)
			
		«ENDFOR»
		## Workflow
		«FOR t: process.tasks»
			retrive_«t.name.toLowerCase»_operator «IF t.next != null »>>retrive_«t.next.type.name»_operator«ENDIF»
		«ENDFOR»
		'''
		
	
}